module.exports = {
	name: "Invite",
	triggers: ["invite"],
	description: "Provides an invite link.",
	category: "misc",
	arguments: {
		positional: [],
		args: []
	},
	func: func
};

function func(message){
	message.channel.send("no invites here, boy");
}
