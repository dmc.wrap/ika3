module.exports = {
	name: "Help",
	triggers: ["help"],
	description: "Help message.",
	category: "misc",
	arguments: {
		positional: [],
		args: []
	},
	func: func
};

function func(message, args){
	message.channel.send("https://ika.eiko.cc/\nYou can also get help for each command with -h or --help.");
}
