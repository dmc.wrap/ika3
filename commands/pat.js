module.exports = {
	name: "Pat",
	triggers: ["pat"],
	description: "Pat someone for those sweet numbers.",
	category: "game",
	arguments: {
		positional: [],
		args: [
			{
				short: "l", long: "leaderboard"
			}
		]
	},
	func: func
};

function func(message, args){

	let out = "";

	if(args.l || args.leaderboard){
		DB.all("SELECT id, pats FROM headpats ORDER BY pats DESC LIMIT 10").then(res => {
			DB.get("SELECT TOTAL(pats), AVG(pats), COUNT(pats) from headpats").then(sum => { // need to reduce to one query if possible

				embed.title = sum["TOTAL(pats)"]+" total pats, "+sum["COUNT(pats)"]+" patters, "+sum["AVG(pats)"].toFixed(1)+" average.";

				for(var i = 0; i < res.length; i += 1){
					out += "#"+(i+1)+" "+((Bot.users.get(res[i].id)||{}).username||"?")+": "+res[i].pats;
				}

				message.channel.send(out);
			});
		});

		return;
	}

	DB.get("SELECT pats FROM headpats WHERE id=?", message.author.id).then(res => {

		let pats = res ? res.pats + 1 : 1;

		DB.run("REPLACE INTO headpats (id, pats) VALUES (?, ?)", message.author.id, pats).then(() => {
			out += message.author.name+" has given "+pats+" headpats.\n";

			message.channel.send(out);
		});
	});
}