module.exports = {
	name: "Roll",
	triggers: ["roll", "random", "reroll"],
	description: "Rolls a random number.",
	category: "game",
	arguments: {
		positional: ["string"],
		args: []
	},
	func: func
};

let congrats = ['Singles', 'Dubs', 'Trips!', 'Quads!!', 'Quints!!!', 'Holy shit ur a god!!!1!1!'];

function func(message, args){

	let roll = Math.floor(Math.random() * 999999999),
		streak = roll.toString().match(/(.)\1*$/)[0].length;

	out = (streak < 6 ? congrats[streak - 1] : congrats[5])+"\n"+roll;
	message.channel.send(out);
}
