module.exports = {
	name: "Ping",
	triggers: ["ping", "uptime"],
	description: "Gives bot ping and uptime.",
	category: "general",
	arguments: {
		positional: [],
		args: []
	},
	func: func
};

function func(message){

	let out = "Ping: "+Math.round(Bot.ping)+"ms"+"\nUptime: "+Utility.toHHMMSS(process.uptime())+"\nFeeling good with " + Object.values(Commands).filter((v, i, s) => {return s.indexOf(v) == i}).length + " commands loaded.";

	message.channel.send(out);
}
