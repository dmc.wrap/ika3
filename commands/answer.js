module.exports = {
	name: "Answer",
	triggers: ["answer", "ans"],
	description: "Answers a question.",
	category: "game",
	arguments: {
		positional: ["question"],
		args: []
	},
	func: func
};

function func(message){
	message.reply(Math.random > 0.5 ? "Yes." : "No.");
}