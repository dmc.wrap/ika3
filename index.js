process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
global.DMC = require("../dmc.wrap");
const Minimist = require("./minimist.js");
const Fs = require("fs");

global.Utility = require("./utility.js");
global.Config = JSON.parse(Fs.readFileSync("config.json"))

global.Images = Utility.getImageLists();

global.Commands = Utility.getCommands();

global.Bot = new DMC.Client(Config.key, Config.login);

global.guildConfigs;
global.DB;

Bot.on("ready", () => {
	console.log("ready");
	Utility.initDB();
});

/*Bot.on("guildMemberAdd", (member) => {
	if(guildConfigs[member.guild.id].mutes[member.id])
		Utility.checkUserMute(guildConfigs[member.guild.id].mutes[member.id], member.guild.id, member.id);
});*/

Bot.on("newmsg", (message) => {
	if(!message.content.startsWith(Config.trigger))
		return;

	let args = Minimist(message.content.split(" "));
	let command = args._.splice(0, 1)[0].slice(Config.trigger.length).toLowerCase();

	console.log(command, args);

	let cmd = Commands[command];

	if(cmd){
		if(args.h || args.help){
			let out = cmd.name+"\n"+cmd.description;
			out += "\n\nTriggers\n"+cmd.triggers.map(t => {return Config.trigger + t}).join(", ");

			if(cmd.arguments.positional[0])
				out += "Arguments\n"+cmd.arguments.positional.join(" ");
			if(cmd.arguments.args[0])
				out += "Flags\n"+cmd.arguments.args.map(a => {return "-"+a.short+" --"+a.long}).join(", ");

			return message.reply(out);
		}

		if(cmd.category === "owner" && message.author.id != Config.ownerId)
			return;

		if(message.guild && guildConfigs[message.guild.id].disabledcommands && guildConfigs[message.guild.id].disabledcommands.includes(cmd.name.toLowerCase()) && cmd.category !== "owner")
			return;

		try{
			cmd.func(message, args, command);
		} catch(err){
			console.error(err);

			message.reply(err.name);
		}
	}
});
